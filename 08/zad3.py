#!/usr/bin/env python
import socket, sys, threading
import random
import math

num = math.floor(21 + random.random() * (37 - 21 + 1))
print(num)

class ClientThread(threading.Thread):
    def __init__(self, connection):
        threading.Thread.__init__(self)
        # ...
        self.sock, self.address = connection

    def run(self):
        # obsluga odbierania i wysylania danych
        while True:
            try:
                data = self.sock.recv(4096)
                if data:
                    if int(data) < num:
                        self.sock.send(b'Mniejsze')
                    elif int(data) > num:
                        self.sock.send(b'Wieksze')
                    else:
                        self.sock.send(b'Rowne')

            except ValueError:
                self.sock.send(b'Blad')
            except Exception:
                self.sock.close()
                print (f"{self.address} offline")
                break

class Server:
    def __init__(self, ip, port):
        # ...
        self.ip = ip
        self.port = port

    def run(self):
        try:
            # socket, bind, listen
            self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            self.socket.bind((self.ip, self.port))
            self.socket.listen(10)
            print (f"Bound to {self.ip}:{self.port}")

            while True:
                # accept
                connection = self.socket.accept()
                c = ClientThread(connection)
                c.start()
                print (f"Accepted {c.address}")
        except:
            # ...
            self.socket.close()

if __name__ == '__main__':
    s = Server('127.0.0.1', 6666)
    s.run()
