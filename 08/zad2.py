#!/usr/bin/env python
import socket, sys, threading

file = open("log.txt", "a")

class ClientThread(threading.Thread):
    def __init__(self, connection):
        threading.Thread.__init__(self)
        # ...
        self.sock, self.address = connection

    def run(self):
        # obsluga odbierania i wysylania danych
        while True:
            try:
                data = self.sock.recv(4096)
                if data:
                    self.sock.send(data)
                    file.write (f"Sending back to client {self.address} data: {data}\n")
            except:
                self.sock.close()
                file.write (f"Client {self.address} disconnected\n")
                break

class Server:
    def __init__(self, ip, port):
        # ...
        self.ip = ip
        self.port = port

    def run(self):
        try:
            # socket, bind, listen
            self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            self.socket.bind((self.ip, self.port))
            self.socket.listen(10)

            file.write (f"Server listening on {self.ip}:{self.port}\n")

            while True:
                # accept
                connection = self.socket.accept()
                c = ClientThread(connection)
                c.start()
                file.write (f"Client {c.address} connected\n")
        except:
            # ...
            self.socket.close()
            file.close()

if __name__ == '__main__':
    s = Server('127.0.0.1', 6666)
    s.run()
