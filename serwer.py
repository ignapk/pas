#!/usr/bin/env python3
import socket, threading, random, math, time

def gen_random(_min=12, _max=34):
    return str(math.floor(_min + random.random() * (_max - _min + 1))).encode()

class UDPThread(threading.Thread):
    def __init__(self, udp):
        threading.Thread.__init__(self)
        self.udp = udp

    def run(self):
        while True:
            data, address = self.udp.recvfrom(4096)
            print (f"UDPThread: Received {data} from udp {address}")
            time.sleep(1)
            self.udp.sendto(str(int(data) * 2).encode(), address)
            print (f"UDPThread: Sent {int(data) * 2} to udp {address}")
            time.sleep(1)

class ClientThread(threading.Thread):
    def __init__(self, connection, udp):
        threading.Thread.__init__(self)
        self.tcp, self.address = connection
        self.udp = udp

    def run(self):
        data = [gen_random() for _ in range(3)]
        for elem in data:
            self.tcp.send(elem)
        print (f"Sent to tcp {self.address} data {data}")

        time.sleep(1)

        data = [self.tcp.recv(2) for _ in range(3)]
        print (f"Received from tcp {self.address} data {data}")

        time.sleep(1)

        data = [str(int(elem) * 2).encode() for elem in data]
        for elem in data:
            self.udp.sendto(elem, self.address)
        print (f"Sent to udp {self.address} data {data}")

        time.sleep(1)

class Server:
    def __init__(self, ip, port):
        self.ip = ip
        self.port = port

    def run(self):
        try:
            self.tcp = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.tcp.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            self.tcp.bind((self.ip, self.port))
            self.tcp.listen(10)
            print (f"Bound tcp socket to {self.ip}:{self.port}")

            self.udp = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            self.udp.bind((self.ip, self.port))
            u = UDPThread(self.udp)
            u.start()

            while True:
                connection = self.tcp.accept()
                c = ClientThread(connection, self.udp)
                c.start()
                print (f"Accepted {c.address}")
        except Exception as e:
            print (f"{e=}")
            self.tcp.close()
            self.udp.close()

if __name__ == '__main__':
    s = Server('127.0.0.1', 6666)
    s.run()
