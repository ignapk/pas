#!/usr/bin/env python3
import socket, random, math, time

def gen_random(_min=12, _max=34):
    return str(math.floor(_min + random.random() * (_max - _min + 1))).encode()

if __name__ == "__main__":
    tcp = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    tcp.connect(("127.0.0.1", 6666))
    print (f"Connected to tcp {tcp.getpeername()} as {tcp.getsockname()}")

    data = [gen_random() for _ in range(3)]
    for elem in data:
        tcp.send(elem)
    print (f"Sent to tcp {tcp.getpeername()} data {data}")

    data = [tcp.recv(2) for _ in range(3)]
    print (f"Received from tcp {tcp.getpeername()} data {data}")

    udp = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    udp.bind(tcp.getsockname())
    print (f"Created udp socket at {udp.getsockname()}")

    data = [str(int(elem) * 5).encode() for elem in data]
    for elem in data:
        udp.sendto(elem, tcp.getpeername())
    print (f"Sent to udp {tcp.getpeername()} data {data}")

    tcp.close()

    while True:
        data, address = udp.recvfrom(4096)
        print (f"Received from udp {address} data {data}")
        udp.sendto(str(int(data) * 5).encode(), address)
        print (f"Sent to udp {address} data {int(data) * 5}")

    udp.close()
