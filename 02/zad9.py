import socket
import struct
import time

s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

try:
    s.sendto(bytes(socket.gethostbyname(socket.gethostname()), "utf-8"), ("127.0.0.1", 2906))
    print("Wynik to: " + str(s.recv(1024)))
except socket.error as e:
    print(f"Error: {e}")

s.close()
