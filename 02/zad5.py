import socket
import struct
import time

s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

try:
    while True:
        s.sendto(bytes(input(), 'utf-8'), ("127.0.0.1", 2901))
        print(s.recv(1024))
except socket.error as e:
    print(f"Error: {e}")

s.close()
