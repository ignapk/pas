import sys
import ipaddress
import socket

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

try:
    sock.connect((sys.argv[1], int(sys.argv[2])))
    print(f"Connected to server {socket.getservbyport(int(sys.argv[2]))}.")
except IndexError:
    print(f"Usage: {sys.argv[0]} ip port")
