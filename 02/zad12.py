import socket
import struct
import time

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

try:
    s.connect(("127.0.0.1", 2908))
    data = b"Hello there"
    #data = b"Hello there0123456789012345678901234567890"
    if len(data) < 20:
        data = data.ljust(20)
    else:
        data = data[:20]
    s.send(data)
    print(s.recv(20))
except socket.error as e:
    print(f"Error: {e}")

s.close()
