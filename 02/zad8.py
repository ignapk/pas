import sys
import ipaddress
import socket


for i in range(1, 65535):
    try:
        #print(i)
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.settimeout(0.1)
        sock.connect((sys.argv[1], i))
        print(f"Port {i} is open: {socket.getservbyport(i)}")
    except TimeoutError:
        sock.close()
        continue
    except IndexError:
        print(f"Usage: {sys.argv[0]} ip")
        break
