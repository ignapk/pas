import socket
import struct
import time

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

try:
    s.connect(("127.0.0.1", 2900))
    while True:
        s.send(bytes(input(), 'utf-8'))
        print(s.recv(1024))
except socket.error as e:
    print(f"Error: {e}")

s.close()
