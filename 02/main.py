#==> zad1.py <==
import socket
import struct
import time

s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
gowno=b'\x1b' + 47 * b'\0'
s.sendto(gowno, ("ntp.task.gda.pl", 123))
data = struct.unpack('!12I', s.recv(1024))[10] - 2208988800
print(time.ctime(data))
s.close()

#==> zad2.py <==
import socket
import struct
import time

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

try:
    s.connect(("127.0.0.1", 2900))
    s.send(b"Hello there")
    print(s.recv(1024))
except socket.error as e:
    print(f"Error: {e}")

s.close()

#==> zad3.py <==
import socket
import struct
import time

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

try:
    s.connect(("127.0.0.1", 2900))
    while True:
        s.send(bytes(input(), 'utf-8'))
        print(s.recv(1024))
except socket.error as e:
    print(f"Error: {e}")

s.close()

#==> zad4.py <==
import socket
import struct
import time

s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

try:
    s.sendto(b"Hello", ("127.0.0.1", 2901))
    print(s.recv(1024))
except socket.error as e:
    print(f"Error: {e}")

s.close()

#==> zad5.py <==
import socket
import struct
import time

s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

try:
    while True:
        s.sendto(bytes(input(), 'utf-8'), ("127.0.0.1", 2901))
        print(s.recv(1024))
except socket.error as e:
    print(f"Error: {e}")

s.close()

#==> zad6.py <==
import socket
import struct
import time

s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

try:
    s.sendto(bytes(input("Podaj pierwsza liczbe:"), 'utf-8'), ("127.0.0.1", 2902))
    s.sendto(bytes(input("Podaj operator:"), 'utf-8'), ("127.0.0.1", 2902))
    s.sendto(bytes(input("Podaj druga liczbe:"), 'utf-8'), ("127.0.0.1", 2902))
    print("Wynik to: " + str(s.recv(1024)))
except socket.error as e:
    print(f"Error: {e}")

s.close()

#==> zad7.py <==
import sys
import ipaddress
import socket

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

try:
    sock.connect((sys.argv[1], int(sys.argv[2])))
    print(f"Connected to server {socket.getservbyport(int(sys.argv[2]))}.")
except IndexError:
    print(f"Usage: {sys.argv[0]} ip port")

#==> zad8.py <==
import sys
import ipaddress
import socket


for i in range(1, 65535):
    try:
        #print(i)
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.settimeout(0.1)
        sock.connect((sys.argv[1], i))
        print(f"Port {i} is open: {socket.getservbyport(i)}")
    except TimeoutError:
        sock.close()
        continue
    except IndexError:
        print(f"Usage: {sys.argv[0]} ip")
        break

#==> zad9.py <==
import socket
import struct
import time

s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

try:
    s.sendto(bytes(socket.gethostbyname(socket.gethostname()), "utf-8"), ("127.0.0.1", 2906))
    print("Wynik to: " + str(s.recv(1024)))
except socket.error as e:
    print(f"Error: {e}")

s.close()

#==> zad10.py <==
import socket
import struct
import time

s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

try:
    s.sendto(bytes(socket.gethostname(), "utf-8"), ("127.0.0.1", 2907))
    print("Wynik to: " + str(s.recv(1024)))
except socket.error as e:
    print(f"Error: {e}")

s.close()

#==> zad11.py <==
import socket
import struct
import time

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

try:
    s.connect(("127.0.0.1", 2908))
    data = b"Hello there"
    #data = b"Hello there0123456789012345678901234567890"
    if len(data) < 20:
        data = data.ljust(20)
    else:
        data = data[:20]
    s.send(data)
    print(s.recv(20))
except socket.error as e:
    print(f"Error: {e}")

s.close()

#==> zad12.py <==
import socket
import struct
import time

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

try:
    s.connect(("127.0.0.1", 2908))
    data = b"Hello there"
    #data = b"Hello there0123456789012345678901234567890"
    if len(data) < 20:
        data = data.ljust(20)
    else:
        data = data[:20]
    s.send(data)
    print(s.recv(20))
except socket.error as e:
    print(f"Error: {e}")

s.close()
