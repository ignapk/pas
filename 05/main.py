#==> zad1.py <==
import requests

headers = {'user-agent': 'Safari 7.0.3'}
r = requests.get('http://httpbin.org:80/html', headers=headers)

file = open("output.html", "w")
file.write(r.text)
file.close()

#==> zad2.py <==
import requests

r = requests.get('http://httpbin.org:80/image/png')

image = open('output.png', 'wb')
image.write(r.content)
image.close()

#==> zad4.py <==
import requests

form_data = {}
form_data['custname'] = input("Customer name:")
form_data['custtel'] = input("Customer telephon:")
form_data['custemail'] = input("Customer email:")
form_data['size'] = input("Size:")
form_data['topping'] = input("Topping:")
form_data['delivery'] = input("Delivery:")
form_data['comments'] = input("Comments:")

r = requests.post('http://httpbin.org/post', data=form_data)

print(r.text)
