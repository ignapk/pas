import requests

headers = {'user-agent': 'Safari 7.0.3'}
r = requests.get('http://httpbin.org:80/html', headers=headers)

file = open("output.html", "w")
file.write(r.text)
file.close()
