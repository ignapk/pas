#==> zad1.py <==
import socket
from datetime import datetime

server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
server.bind(('127.0.0.1', 21337))
server.listen(10)

while True:
    client, address = server.accept()
    print (f"Connected to {address}")

    data = client.recv(4096)
    print (f"Received {data}")

    current = datetime.now()
    client.send(str(current).encode())
    print (f"Sent {current}")

    client.close()
    print (f"Disconnected from {address}")

server.close()

#==> zad2.py <==
import socket
from datetime import datetime

server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
server.bind(('127.0.0.1', 21337))
server.listen(10)

while True:
    client, address = server.accept()
    print (f"Connected to {address}")

    data = client.recv(4096)
    print (f"Received {data}")

    client.send(data)
    print (f"Sent {data}")

    client.close()
    print (f"Disconnected from {address}")

server.close()

#==> zad3.py <==
import socket
from datetime import datetime

server = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

server.bind(('127.0.0.1', 21337))

while True:
    data, address = server.recvfrom(4096)
    print (f"Received {data} from {address}")

    server.sendto(data, address)
    print (f"Sent {data} to {address}")

server.close()

#==> zad4.py <==
import socket
from datetime import datetime

server = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

server.bind(('127.0.0.1', 21337))

while True:
    data, address = server.recvfrom(4096)
    print (f"Received {data} from {address}")
    a = data

    data, address = server.recvfrom(4096)
    print (f"Received {data} from {address}")
    op = data

    data, address = server.recvfrom(4096)
    print (f"Received {data} from {address}")
    b = data

    data = str(eval(a+op+b)).encode()
    server.sendto(data, address)
    print (f"Sent {data} to {address}")

server.close()

#==> zad5.py <==
import socket
from datetime import datetime

server = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

server.bind(('127.0.0.1', 21337))

while True:
    data, address = server.recvfrom(4096)
    print (f"Received {data} from {address}")

    data = socket.gethostbyaddr(data)[0].encode()
    server.sendto(data, address)
    print (f"Sent {data} to {address}")

server.close()

#==> zad6.py <==
import socket
from datetime import datetime

server = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

server.bind(('127.0.0.1', 21337))

while True:
    data, address = server.recvfrom(4096)
    print (f"Received {data} from {address}")

    data = socket.gethostbyname(data).encode()
    server.sendto(data, address)
    print (f"Sent {data} to {address}")

server.close()

#==> zad7.py <==
 #!/usr/bin/env python

import socket, select
from time import gmtime, strftime

HOST = '0.0.0.0'
PORT = 2900

connected_clients_sockets = []

server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
server_socket.bind((HOST, PORT))
server_socket.listen(10)

connected_clients_sockets.append(server_socket)

print "[%s] TCP ECHO Server is waiting for incoming connections on port %s ... " % (strftime("%Y-%m-%d %H:%M:%S", gmtime()), PORT)

while True:

    read_sockets, write_sockets, error_sockets = select.select(connected_clients_sockets, [], [])

    for sock in read_sockets:

        if sock == server_socket:

            sockfd, client_address = server_socket.accept()
            connected_clients_sockets.append(sockfd)

            print "[%s] Client %s connected ... " % (strftime("%Y-%m-%d %H:%M:%S", gmtime()), client_address)

        else:
            try:
                data = sock.recv(20)
                if data:
                    sock.send(data)
                    print "[%s] Sending back to client %s data: [\'%s\']... " % (strftime("%Y-%m-%d %H:%M:%S", gmtime()), client_address, data)

            except:
                print "[%s] Client (%s) is offline" % (strftime("%Y-%m-%d %H:%M:%S", gmtime()), client_address)
                sock.close()
                connected_clients_sockets.remove(sock)
                continue
server_socket.close()

#==> zad8.py <==
 #!/usr/bin/env python

import socket, select
from time import gmtime, strftime

HOST = '0.0.0.0'
PORT = 2900

connected_clients_sockets = []

server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
server_socket.bind((HOST, PORT))
server_socket.listen(10)

connected_clients_sockets.append(server_socket)

print "[%s] TCP ECHO Server is waiting for incoming connections on port %s ... " % (strftime("%Y-%m-%d %H:%M:%S", gmtime()), PORT)

while True:

    read_sockets, write_sockets, error_sockets = select.select(connected_clients_sockets, [], [])

    for sock in read_sockets:

        if sock == server_socket:

            sockfd, client_address = server_socket.accept()
            connected_clients_sockets.append(sockfd)

            print "[%s] Client %s connected ... " % (strftime("%Y-%m-%d %H:%M:%S", gmtime()), client_address)

        else:
            try:
                data = sock.recv(20)
                if data:
                    sock.send(data)
                    print "[%s] Sending back to client %s data: [\'%s\']... " % (strftime("%Y-%m-%d %H:%M:%S", gmtime()), client_address, data)

            except:
                print "[%s] Client (%s) is offline" % (strftime("%Y-%m-%d %H:%M:%S", gmtime()), client_address)
                sock.close()
                connected_clients_sockets.remove(sock)
                continue
server_socket.close()

#==> zad9.py <==
import socket

def check_msg_syntax(txt):
    s = len(txt.split(";"))
    if s != 7:
        return "BAD_SYNTAX"
    else:
        tmp = txt.split(";")
        if tmp[0] == "zad14odp" and tmp[1] == "src" and tmp[3] == "dst" and tmp[5] == "data":
            try :
                src_port = int(tmp[2])
                dst_port = int(tmp[4])
                data = tmp[6]
            except :
                return "BAD_SYNTAX"
            if src_port == 60788 and dst_port == 2901 and data == "programming in python is fun":
                return "TAK"
            else:
                return "NIE"
        else:
            return "BAD_SYNTAX"

server = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

server.bind(('127.0.0.1', 21337))

try:
    while True:
        data, address = server.recvfrom(1024)
        print (f"Received {data} from {address}")

        data = check_msg_syntax(data.decode())

        server.sendto(data.encode(), address)
        print (f"Sent {data} to {address}")
finally:
    server.close()

#==> zad10.py <==
import socket

def check_msg_syntax(txt):
    s = len(txt.split(";"))
    if s != 7:
        return "BAD_SYNTAX"
    else:
        tmp = txt.split(";")
        if tmp[0] == "zad13odp" and tmp[1] == "src" and tmp[3] == "dst" and tmp[5] == "data":
            try:
                src_port = int(tmp[2])
                dst_port = int(tmp[4])
                data = tmp[6]
            except :
                return "BAD_SYNTAX:"
            if src_port == 2900 and dst_port == 35211 and data == "hello :)":
                return "TAK"
            else:
                return "NIE"
        else:
            return "BAD_SYNTAX"

server = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

server.bind(('127.0.0.1', 21337))

try:
    while True:
        data, address = server.recvfrom(1024)
        print (f"Received {data} from {address}")

        data = check_msg_syntax(data.decode())
        
        server.sendto(data.encode(), address)
        print (f"Sent {data} to {address}")
finally:
    server.close()

#==> zad11.py <==
import socket

def check_msgA_syntax(txt):
    s = len(txt.split(";"))
    if s != 9:
        return "BAD_SYNTAX"
    else:
        tmp = txt.split(";")
        if tmp[0] == "zad15odpA" and tmp[1] == "ver" and tmp[3] == "srcip" and tmp[5] == "dstip" and tmp[7] == "type":
            try:
                ver = int(tmp[2])
                srcip = tmp[4]
                dstip = tmp[6]
                type = int(tmp[8])
                if ver == 4 and type == 6 and srcip == "212.182.24.27" and dstip == "192.168.0.2":
                    return "TAK"
                else:
                    return "NIE"
            except:
                return "NIE"
        else:
            return "BAD_SYNTAX"


def check_msgB_syntax(txt):
    s = len(txt.split(";"))
    if s != 7:
        return "BAD_SYNTAX"
    else:
        tmp = txt.split(";")
        if tmp[0] == "zad15odpB" and tmp[1] == "srcport" and tmp[3] == "dstport" and tmp[5] == "data":

            try:
                srcport = int(tmp[2])
                dstport = int(tmp[4])
                data = tmp[6]

                if srcport == 2900 and dstport == 47526 and data == "network programming is fun":
                    return "TAK"
                else:
                    return "NIE"
            except:
                return "NIE"
        else:
            return "BAD_SYNTAX"

server = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

server.bind(('127.0.0.1', 21337))

try:
    while True:
        data, address = server.recvfrom(1024)
        print (f"Received {data} from {address}")

        tmp = data.decode().split(";")

        if tmp[0] == "zad15odpA":
            data = check_msgA_syntax(data.decode())
            server.sendto(data.encode(), address)
            print (f"Sent {data} to {address}")

        elif tmp[0] == "zad15odpB":
            data = check_msgB_syntax(data.decode())
            server.sendto(data.encode(), address)
            print (f"Sent {data} to {address}")

        else:
            server.sendto(b'BAD_SYNTAX', address)
            print (f"Sent BAD_SYNTAX to {address}")
finally:
    server.close()
