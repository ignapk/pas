import socket
import struct
import time

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

try:
    s.connect(("127.0.0.1", 2900))
    s.send(b"Hello there")
    print(s.recv(1024))
except socket.error as e:
    print(f"Error: {e}")

s.close()
