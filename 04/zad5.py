import socket
from datetime import datetime

server = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

server.bind(('127.0.0.1', 21337))

while True:
    data, address = server.recvfrom(4096)
    print (f"Received {data} from {address}")

    data = socket.gethostbyaddr(data)[0].encode()
    server.sendto(data, address)
    print (f"Sent {data} to {address}")

server.close()
