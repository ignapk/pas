import socket
import struct
import time

s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

try:
    s.sendto(b"Hello", ("127.0.0.1", 21337))
    print(s.recv(1024))
except socket.error as e:
    print(f"Error: {e}")

s.close()
