import socket
from datetime import datetime

server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
server.bind(('127.0.0.1', 21337))
server.listen(10)

while True:
    client, address = server.accept()
    print (f"Connected to {address}")

    data = client.recv(4096)
    print (f"Received {data}")

    client.send(data)
    print (f"Sent {data}")

    client.close()
    print (f"Disconnected from {address}")

server.close()
