import socket

def check_msgA_syntax(txt):
    s = len(txt.split(";"))
    if s != 9:
        return "BAD_SYNTAX"
    else:
        tmp = txt.split(";")
        if tmp[0] == "zad15odpA" and tmp[1] == "ver" and tmp[3] == "srcip" and tmp[5] == "dstip" and tmp[7] == "type":
            try:
                ver = int(tmp[2])
                srcip = tmp[4]
                dstip = tmp[6]
                type = int(tmp[8])
                if ver == 4 and type == 6 and srcip == "212.182.24.27" and dstip == "192.168.0.2":
                    return "TAK"
                else:
                    return "NIE"
            except:
                return "NIE"
        else:
            return "BAD_SYNTAX"


def check_msgB_syntax(txt):
    s = len(txt.split(";"))
    if s != 7:
        return "BAD_SYNTAX"
    else:
        tmp = txt.split(";")
        if tmp[0] == "zad15odpB" and tmp[1] == "srcport" and tmp[3] == "dstport" and tmp[5] == "data":

            try:
                srcport = int(tmp[2])
                dstport = int(tmp[4])
                data = tmp[6]

                if srcport == 2900 and dstport == 47526 and data == "network programming is fun":
                    return "TAK"
                else:
                    return "NIE"
            except:
                return "NIE"
        else:
            return "BAD_SYNTAX"

server = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

server.bind(('127.0.0.1', 21337))

try:
    while True:
        data, address = server.recvfrom(1024)
        print (f"Received {data} from {address}")

        tmp = data.decode().split(";")

        if tmp[0] == "zad15odpA":
            data = check_msgA_syntax(data.decode())
            server.sendto(data.encode(), address)
            print (f"Sent {data} to {address}")

        elif tmp[0] == "zad15odpB":
            data = check_msgB_syntax(data.decode())
            server.sendto(data.encode(), address)
            print (f"Sent {data} to {address}")

        else:
            server.sendto(b'BAD_SYNTAX', address)
            print (f"Sent BAD_SYNTAX to {address}")
finally:
    server.close()
