import socket

s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

datagram = bytes.fromhex("0b 54 89 8b 1f 9a 18 ec bb b1 64 f2 80 18 00 e3 67 71 00 00 01 01 08 0a 02 c1 a4 ee 00 1a 4c ee 68 65 6c 6c 6f 20 3a 29")

src = bytes(str(int.from_bytes(datagram[:2])), "utf-8")
dst = bytes(str(int.from_bytes(datagram[2:4])), "utf-8")
hlen = datagram[12] >> 4
data = datagram[24 + hlen:]

msg = bytes("zad13odp;src;", "utf-8") + src + bytes(";dst;", "utf-8") + dst + bytes(";data;", "utf-8") + data

s.sendto(msg, ("127.0.0.1", 2909))

print(s.recv(1024))

s.close()
