import socket

s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

datagram = bytes.fromhex("ed 74 0b 55 00 24 ef fd 70 72 6f 67 72 61 6d 6d 69 6e 67 20 69 6e 20 70 79 74 68 6f 6e 20 69 73 20 66 75 6e")

src = bytes(str(int.from_bytes(datagram[:2])), "utf-8")
dst = bytes(str(int.from_bytes(datagram[2:4])), "utf-8")
data = datagram[8:]

msg = bytes("zad14odp;src;", "utf-8") + src + bytes(";dst;", "utf-8") + dst + bytes(";data;", "utf-8") + data

s.sendto(msg, ("127.0.0.1", 2910))

print(s.recv(1024))

s.close()
