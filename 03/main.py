#==> zad13.py <==
import socket

s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

datagram = bytes.fromhex("ed 74 0b 55 00 24 ef fd 70 72 6f 67 72 61 6d 6d 69 6e 67 20 69 6e 20 70 79 74 68 6f 6e 20 69 73 20 66 75 6e")

src = bytes(str(int.from_bytes(datagram[:2])), "utf-8")
dst = bytes(str(int.from_bytes(datagram[2:4])), "utf-8")
data = datagram[8:]

msg = bytes("zad14odp;src;", "utf-8") + src + bytes(";dst;", "utf-8") + dst + bytes(";data;", "utf-8") + data

s.sendto(msg, ("127.0.0.1", 2910))

print(s.recv(1024))

s.close()

#==> zad14.py <==
import socket

s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

datagram = bytes.fromhex("0b 54 89 8b 1f 9a 18 ec bb b1 64 f2 80 18 00 e3 67 71 00 00 01 01 08 0a 02 c1 a4 ee 00 1a 4c ee 68 65 6c 6c 6f 20 3a 29")

src = bytes(str(int.from_bytes(datagram[:2])), "utf-8")
dst = bytes(str(int.from_bytes(datagram[2:4])), "utf-8")
hlen = datagram[12] >> 4
data = datagram[24 + hlen:]

msg = bytes("zad13odp;src;", "utf-8") + src + bytes(";dst;", "utf-8") + dst + bytes(";data;", "utf-8") + data

s.sendto(msg, ("127.0.0.1", 2909))

print(s.recv(1024))

s.close()

#==> zad15.py <==
import socket

s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

datagram = bytes.fromhex("45 00 00 4e f7 fa 40 00 38 06 9d 33 d4 b6 18 1b c0 a8 00 02 0b 54 b9 a6 fb f9 3c 57 c1 0a 06 c1 80 18 00 e3 ce 9c 00 00 01 01 08 0a 03 a6 eb 01 00 0b f8 e5 6e 65 74 77 6f 72 6b 20 70 72 6f 67 72 61 6d 6d 69 6e 67 20 69 73 20 66 75 6e")

ver = bytes(str(datagram[0] >> 4), "utf-8")
srcip = bytes(socket.inet_ntoa(datagram[12:16]), "utf-8")
dstip = bytes(socket.inet_ntoa(datagram[16:20]), "utf-8")
type = bytes(str(datagram[9]), "utf-8")


msg = bytes("zad15odpA;ver;", "utf-8") + ver + bytes(";srcip;", "utf-8") + srcip + bytes(";dstip;", "utf-8") + dstip + bytes(";type;", "utf-8") + type

s.sendto(msg, ("127.0.0.1", 2911))

if s.recv(1024) != b'TAK':
    s.close()
    exit(1)

srcport = bytes(str(int.from_bytes(datagram[20:22])), "utf-8")
dstport = bytes(str(int.from_bytes(datagram[22:24])), "utf-8")
tcpoptlen = 12
data = datagram[40 + tcpoptlen:]

msg = bytes("zad15odpB;srcport;", "utf-8") + srcport + bytes(";dstport;", "utf-8") + dstport + bytes(";data;", "utf-8") + data

s.sendto(msg, ("127.0.0.1", 2911))

print(s.recv(1024))
s.close()
