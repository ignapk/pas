import sys
import ipaddress
import socket

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

try:
    sock.connect((sys.argv[1], int(sys.argv[2])))
    print("Connected to server")
except IndexError:
    print(f"Usage: {sys.argv[0]} ip port")
