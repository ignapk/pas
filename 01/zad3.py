import ipaddress

address = input()

try:
    ip = ipaddress.ip_address(address)
    print(f"{ip} is a correct IP address")
except ValueError:
    print(f"{address} is not a correct IP address")
