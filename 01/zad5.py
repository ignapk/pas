import sys
import socket

try:
    print(socket.gethostbyname(sys.argv[1].strip()))
except ValueError:
    print(f"{sys.argv[1]} is not a correct hostname")
except:
    print(f"Usage: {sys.argv[0]} hostname")
