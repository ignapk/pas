import sys
import ipaddress
import socket

try:
    ip = ipaddress.ip_address(sys.argv[1])
    #print(f"{ip} is a correct IP address")
    print(socket.gethostbyaddr(ip))
except ValueError:
    print(f"{sys.argv[1]} is not a correct IP address")
except:
    print(f"Usage: {sys.argv[0]} ip")
