#############
#Zad 1
import shutil

name = input()

shutil.copyfile(name, "lab1zad1.txt")


#############
#Zad 2
import shutil

name = input()

shutil.copyfile(name, "lab1zad1.png")

#############
#Zad 3
import ipaddress

address = input()

try:
    ip = ipaddress.ip_address(address)
    print(f"{ip} is a correct IP address")
except ValueError:
    print(f"{address} is not a correct IP address")

#############
#Zad 4
import sys
import ipaddress
import socket

try:
    ip = ipaddress.ip_address(sys.argv[1])
    #print(f"{ip} is a correct IP address")
    print(socket.gethostbyaddr(ip))
except ValueError:
    print(f"{sys.argv[1]} is not a correct IP address")
except:
    print(f"Usage: {sys.argv[0]} ip")

#############
#Zad 5
import sys
import socket

try:
    print(socket.gethostbyname(sys.argv[1].strip()))
except ValueError:
    print(f"{sys.argv[1]} is not a correct hostname")
except:
    print(f"Usage: {sys.argv[0]} hostname")

#############
#Zad 6
import sys
import ipaddress
import socket

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

try:
    sock.connect((sys.argv[1], int(sys.argv[2])))
    print("Connected to server")
except IndexError:
    print(f"Usage: {sys.argv[0]} ip port")

#############
#Zad 7
import sys
import ipaddress
import socket


for i in range(1, 65535):
    try:
        print(i)
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.settimeout(0.1)
        sock.connect((sys.argv[1], i))
        print(f"Port {i} is open")
    except TimeoutError:
        sock.close()
        continue
    except IndexError:
        print(f"Usage: {sys.argv[0]} ip port")
        break
